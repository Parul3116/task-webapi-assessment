﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TaskApi.ITaskRepository;
using TaskApi.Models;
using Task = TaskApi.Models.Task;



// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860



namespace TaskApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class TaskController : ControllerBase
    {
        // Add required dependencies here



        ITask _repo;
        public TaskController(ITask repo)
        {
            _repo = repo;
        }
        [HttpGet]
        public IActionResult GetTasks()
        {
            if (_repo.GetTasks().ToList().Count == 0)
                return NotFound();
            else





                return Ok(_repo.GetTasks());
        }





        [HttpGet("{id}")]





        public IActionResult GetTaskById(int id)
        {
            if (_repo.GetTaskById(id) == null)
                return NotFound("There is no Task");
            else
                return Ok(_repo.GetTaskById(id));
        }





        [HttpPost]
        public IActionResult AddTask(Task task)
        {
            _repo.AddTask(task);
            return Created("Created", task);
        }





        [HttpPut("{id}")]
        public IActionResult EditTask(int id, Task task)
        {
            if (_repo.GetTaskById(id) == null)
            {
                return NotFound("There is no Task");
            }
            else
            {
                _repo.EditTask(id, task);
                return Ok(task);
            }





        }
        [HttpDelete("{id}")]





        public IActionResult DeleteTask(int id)
        {
            if (_repo.GetTaskById(id) == null)
            {
                return NotFound("There is no Task");
            }
            else
            {
                _repo.DeleteTask(id);
                return Ok("Deleted");
            }






        }
    }
}
