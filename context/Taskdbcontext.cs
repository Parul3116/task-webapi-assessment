﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using TaskApi.Models;
using Task = TaskApi.Models.Task;

namespace TaskApi.Context
{
    public class TaskDbContext : DbContext
    {
        public TaskDbContext()
        {



        }



        public TaskDbContext(DbContextOptions<TaskDbContext> options) : base(options) { }



        public DbSet<Task> Tasks { get; set; }



        public DbSet<SubTask> SubTasks { get; set; }

        public DbSet<User> Users { get; set; }
    }




}