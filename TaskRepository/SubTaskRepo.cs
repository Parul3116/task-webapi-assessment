﻿using TaskApi.ITaskRepository;
using TaskApi.Models;
using TaskApi.Context;
using Microsoft.EntityFrameworkCore;







namespace TaskApi.TaskRepository



{



    public class SubTaskRepo : ISubTask



    {



        TaskDbContext _context;



        public SubTaskRepo(TaskDbContext context)



        {



            _context = context;



        }



        public List<SubTask> GetSubTasksByTask(int id)



        {



            return _context.SubTasks.Where(x => x.Id == id).ToList();





        }





        public void AddSubTask(SubTask sbtsk)
        {



            _context.SubTasks.Add(sbtsk);



            _context.SaveChanges();



        }



    }



}



