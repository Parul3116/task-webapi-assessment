﻿using System.Threading.Tasks;
using TaskApi.Context;
using TaskApi.ITaskRepository;
using TaskApi.Models;
using Task = TaskApi.Models.Task;

namespace TaskApi.TaskRepository
{
    public class TaskRepo : ITask
    {
        TaskDbContext _context;
        public TaskRepo(TaskDbContext context)
        {
            _context = context;
        }
        public void AddTask(Task task)
        {
            _context.Tasks.Add(task);
            _context.SaveChanges();
        }





        public void DeleteTask(int id)
        {
            Task task = _context.Tasks.FirstOrDefault(x => x.Id == id);
            if (task != null)
            {
                _context.Tasks.Remove(task);
                _context.SaveChanges();
            }
        }





        public void EditTask(int id, Task task)
        {
            Task task1 = _context.Tasks.FirstOrDefault(x => x.Id == id);
            if (task1 != null)
            {
                _context.Tasks.Update(task);
                _context.SaveChanges();
            }
        }
        public Task GetTaskById(int id)
        {
            Task task = _context.Tasks.FirstOrDefault(x => x.Id == id);
            return task;
        }
        public List<Task> GetTasks()
        {
            return _context.Tasks.ToList();





        }
    }
}