﻿using System.Threading.Tasks;
using TaskApi.Models;
using Task = TaskApi.Models.Task;

namespace TaskApi.ITaskRepository
{
    public interface ITask
    {
        //Declare all following functions only for CRUD for Task & SubTask



        // Add a Task
        // Get all Tasks
        // Get a particular Task
        // Edit some Task
        // Delete some task, in case there is no subtask



        // Add a SubTack for a Task
        // Get all subtasks for a particular Task
        // Here in display Task Name, SubtTask Name,Created By, When



        public List<Task> GetTasks();
        public Task GetTaskById(int id);
        public void AddTask(Task task);
        public void EditTask(int id, Task task);
        public void DeleteTask(int id);







    }
}